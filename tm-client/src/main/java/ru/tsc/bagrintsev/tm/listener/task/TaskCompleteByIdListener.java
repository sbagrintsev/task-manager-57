package ru.tsc.bagrintsev.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String description() {
        return "Complete task by id.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@taskCompleteByIdListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setTaskId(id);
        request.setStatusValue("COMPLETED");
        taskEndpoint.changeTaskStatusById(request);
    }

    @NotNull
    @Override
    public String name() {
        return "task-complete";
    }

}
