package ru.tsc.sbagrintsev.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IAuthService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.model.ITaskService;
import ru.tsc.bagrintsev.tm.api.sevice.model.IUserService;
import ru.tsc.bagrintsev.tm.configuration.ServerConfiguration;
import ru.tsc.bagrintsev.tm.exception.user.LoginAlreadyExistsException;
import ru.tsc.bagrintsev.tm.exception.user.LoginIsIncorrectException;
import ru.tsc.bagrintsev.tm.exception.user.PasswordIsIncorrectException;
import ru.tsc.bagrintsev.tm.model.User;
import ru.tsc.sbagrintsev.tm.marker.DBCategory;

import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;

@Controller
@Category(DBCategory.class)
public abstract class AbstractTest {

    @NotNull
    public static final AnnotationConfigApplicationContext CONTEXT = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    protected static IPropertyService propertyService = CONTEXT.getBean(IPropertyService.class);

    @NotNull
    protected ITaskService taskService = CONTEXT.getBean(ITaskService.class);

    @NotNull
    protected IProjectService projectService = CONTEXT.getBean(IProjectService.class);

    @NotNull
    protected IUserService userService = CONTEXT.getBean(IUserService.class);

    @NotNull
    protected IAuthService authService = CONTEXT.getBean(IAuthService.class);

    @After
    public void destroy() {
        taskService.clearAll();
        projectService.clearAll();
        authService.clearAll();
        userService.clearAll();
    }

    @Before
    public void init() throws LoginIsIncorrectException, GeneralSecurityException, LoginAlreadyExistsException, PasswordIsIncorrectException {
        @NotNull final User user1 = userService.create("test1", "testPassword1");
        user1.setId("testUserId1");
        @NotNull final User user2 = userService.create("test2", "testPassword2");
        user2.setId("testUserId2");
        @NotNull final List<User> list = Arrays.asList(user1, user2);
        userService.clearAll();
        userService.set(list);
    }

}
