package ru.tsc.bagrintsev.tm.api.sevice;

import org.jetbrains.annotations.Nullable;

public interface ITokenService {

    @Nullable
    String getToken();

    void setToken(@Nullable final String token);

}
