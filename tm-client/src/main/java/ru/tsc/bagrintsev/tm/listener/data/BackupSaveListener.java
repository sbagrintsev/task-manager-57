package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.BackupSaveRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class BackupSaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "backup-save";

    @NotNull
    @Override
    public String description() {
        return "Backup current application state in base64 file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@backupSaveListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        domainEndpoint.saveBackup(new BackupSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
