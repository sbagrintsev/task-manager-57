package ru.tsc.bagrintsev.tm.component;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.api.sevice.ILoggerService;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.SystemUtil;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@Component
@RequiredArgsConstructor
public final class Bootstrap {

    @NotNull
    private final ILoggerService loggerService;

    @NotNull
    private final ApplicationEventPublisher publisher;

    private boolean continueExecute = true;

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager-client.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    @SneakyThrows
    private void processOnStart(@Nullable final String[] args) {
        if (args == null || args.length == 0) return;
        @Nullable final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        publisher.publishEvent(new ConsoleEvent(arg));
        publisher.publishEvent(new ConsoleEvent("exit"));
    }

    public void run(@Nullable final String[] args) {
        processOnStart(args);
        initPID();
        showWelcome();
        while (continueExecute) {
            try {
                System.out.print("\nEnter Command:\n>> ");
                @NotNull final String command = TerminalUtil.nextLine();
                publisher.publishEvent(new ConsoleEvent(command));
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                e.printStackTrace();
                System.err.println("[FAIL]");
            }
        }
    }

    private void showWelcome() {
        loggerService.info("*** Welcome to Task Manager ***");
        Runtime.getRuntime().addShutdownHook(new Thread(this::shutdown));
    }

    @SneakyThrows
    private void shutdown() {
        loggerService.info("*** Task Manager is shutting down... ***");
        continueExecute = false;
    }

}
