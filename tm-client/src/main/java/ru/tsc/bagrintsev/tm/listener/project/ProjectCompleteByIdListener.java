package ru.tsc.bagrintsev.tm.listener.project;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

@Component
public class ProjectCompleteByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String description() {
        return "Complete project by id.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@projectCompleteByIdListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        showParameterInfo(EntityField.ID);
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatusValue("COMPLETED");
        projectEndpoint.changeProjectStatusById(request);
    }

    @NotNull
    @Override
    public String name() {
        return "project-complete";
    }

}
