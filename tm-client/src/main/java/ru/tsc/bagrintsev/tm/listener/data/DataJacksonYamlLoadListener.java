package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonYamlLoadRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataJacksonYamlLoadListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-load-yaml";

    @NotNull
    @Override
    public String description() {
        return "Load current application state from yaml file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJacksonYamlLoadListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.loadJacksonYaml(new DataJacksonYamlLoadRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
