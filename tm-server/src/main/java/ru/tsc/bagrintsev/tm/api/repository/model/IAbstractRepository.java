package ru.tsc.bagrintsev.tm.api.repository.model;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    void add(@NotNull final M record);

    void addAll(@NotNull final Collection<M> records);

    void clearAll();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull final String id);

    @NotNull
    EntityManager getEntityManager();

    void setEntityManager(@NotNull EntityManager entityManager);

    long totalCount();

}
