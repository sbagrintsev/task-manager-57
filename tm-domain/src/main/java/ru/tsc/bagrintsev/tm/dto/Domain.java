package ru.tsc.bagrintsev.tm.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import jakarta.xml.bind.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.model.ProjectDTO;
import ru.tsc.bagrintsev.tm.dto.model.TaskDTO;
import ru.tsc.bagrintsev.tm.dto.model.UserDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@XmlRootElement
@NoArgsConstructor
@JsonRootName("domain")
@XmlType(name = "domain")
@XmlAccessorType(XmlAccessType.FIELD)
@JacksonXmlRootElement(localName = "domain")
public final class Domain implements Serializable {

    @JsonProperty("serialVersionUID")
    @XmlElement(name = "serialVersionUID")
    private static final long serialVersionUID = 1;

    @NotNull
    @JsonProperty("id")
    @XmlElement(name = "id")
    private String id = UUID.randomUUID().toString();

    @NotNull
    @JsonProperty("created")
    @XmlElement(name = "created")
    private Date created = new Date();

    @NotNull
    @JsonProperty("user")
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    @JacksonXmlElementWrapper(localName = "users")
    private List<UserDTO> users = new ArrayList<>();

    @NotNull
    @JsonProperty("project")
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    @JacksonXmlElementWrapper(localName = "projects")
    private List<ProjectDTO> projects = new ArrayList<>();

    @NotNull
    @JsonProperty("task")
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    @JacksonXmlElementWrapper(localName = "tasks")
    private List<TaskDTO> tasks = new ArrayList<>();

}
