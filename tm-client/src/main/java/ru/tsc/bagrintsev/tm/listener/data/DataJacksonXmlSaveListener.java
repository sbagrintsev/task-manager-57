package ru.tsc.bagrintsev.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonXmlSaveRequest;
import ru.tsc.bagrintsev.tm.event.ConsoleEvent;

@Component
public final class DataJacksonXmlSaveListener extends AbstractDataListener {

    @NotNull
    @Override
    public String description() {
        return "Save current application state in xml file";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataJacksonXmlSaveListener.name() == #consoleEvent.name")
    public void handle(@NotNull ConsoleEvent consoleEvent) {
        showOperationInfo();
        domainEndpoint.saveJacksonXml(new DataJacksonXmlSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-xml";
    }

}
