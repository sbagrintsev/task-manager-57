package ru.tsc.bagrintsev.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class WrongDateFormatException extends AbstractException {

    public WrongDateFormatException(@Nullable final Throwable cause) {
        super("Error! Wrong date format...", cause);
    }

}
